import { Component, OnChanges, AfterViewInit, Input, ElementRef } from '@angular/core';

declare var window: any;
/**
 * [Component Declaration]
 * param  {selector [hold the tag name to which the component would get rendered]}
 */
@Component({
  selector: 'form-render',
  template: '<div></div>'
})
export class FormRenderComponent implements AfterViewInit, OnChanges {
  // @Input() dom: string;
  @Input() formJson: any;
  @Input() formData: any;
  @Input() config: any;
  renderer: any;
  dom: ElementRef;
  constructor(elem: ElementRef) {
    this.dom = elem.nativeElement;
    this.formData = this.formData;
  }

  /**
   * [ngOnChanges - One of the 'OnChanges' interface method which gets trigger when
   *                the value in the model gets changed. ]
   * @param  changes [Holds the changed data. ]
   *
   */
  ngOnChanges(changes) {
    for (let propName in changes) {
      let change = changes[propName];
      if (!change.isFirstChange()) {
        this.renderer.data = this.formData;
      }
    }
  }

  /**
   * [ngAfterViewInit - One of the 'AfterViewInit' interface that gets triggered
   *                    after the current component gets rendered in the view. ]
   */
  ngAfterViewInit(): void {
    this.renderer = new window.JsonForm(this.dom, this.formJson, this.formData, this.config);
    this.renderer.observable.subscribe((data) => {
      console.log('-=-=-=-=-=-= ', data);
      Object.assign(this.formData, data);
    });
  }

}
