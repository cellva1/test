import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormRenderModule } from '../../src';

import { AppComponent } from './app.component';

@NgModule({
  imports: [
    BrowserModule,
    FormRenderModule,
  ],
  declarations: [
    AppComponent,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
