import { FormRenderComponent } from './app.component';
import { FormRenderModule } from './app.module';

export {
  FormRenderComponent,
  FormRenderModule
}
