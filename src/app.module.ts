import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { FormRenderComponent } from './app.component';


@NgModule({
  declarations: [
    FormRenderComponent
  ],
  imports: [
    BrowserModule
  ],
  exports: [
    FormRenderComponent
  ],
  providers: [],
  bootstrap: [FormRenderComponent]
})
export class FormRenderModule { }
